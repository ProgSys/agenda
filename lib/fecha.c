#include "../include/fecha.h"


fecha* fechaNew(int day, int month, int year)
{
	fecha* date;
	date = (fecha*)malloc(sizeof(fecha));
	date->day = day;
	date->month = month;
	date->year = year;
	return date;
}

void fechaPrint(fecha *date)
{
	printf("Fecha:\t%d/%d/%d \n", date->day, date->month, date->year);
}
