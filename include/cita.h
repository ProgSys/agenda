#include "contacto.h"
#include <string.h>

/*
Constantes para imprimir en diferentes colores
*/
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


/*
Estructura de datos cita la cual cuenta con
char* para:
 - Lugar
 - Hora de Inicio
 - Hora de Fin
un dato tipo fecha para la fecha y un contacto.
en el caso de que la cita no cuente con un contacto,
este tendrá el valor de NULL
*/
typedef struct Tcita{
  char * lugar;
  char * horaInicio;
  char * horaFin;
  fecha * fecha;
  contacto * contacto;
}cita;

/*
esta función reserva la memoria para crear una nueva cita, crea la cita y la retorna
*/
cita * citaNew(char *, char*, char*, fecha*, contacto*);


/*
Estas funciones imprimen una cita apuntada por un Generic el cual se lo pasa como parámetro.
cada una de estas 3 funciones imprime la cita de forma diferenta, más o menos detallada, según se lo requiera
*/
void citaPrint(Generic);
void citaPrint2(Generic);
void citaPrint3(Generic);
/*
*/


/*
Esta función compara dos citas apuntados por punteros Generic, devuelve 
0 si las dos citas tienen el mismo mes y año, y -1 si no coinciden
*/
int citaCmpMY(Generic, Generic);


/*
Esta función compara dos citas apuntados por punteros Generic, devuelve 
0 si las dos citas tienen el numero de día, y -1 si no coinciden
*/
int citaCmpD(Generic, Generic);

/*
Esta función imprime las citas en formato caledario,
recibe como parámetro el número de mes, y la lista 
de citas de ese mes
*/
void calendarioPrint(int, List*);

/*
Esta función compara dos citas apuntados por punteros Generic, devuelve 
0 si las dos citas tienen mismo contacto, y -1 si no coinciden
*/
int citaCmpContact(Generic, Generic);





