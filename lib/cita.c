#include "../include/cita.h"

cita * citaNew(char * lugar, char* horaIni, char* horaFin, fecha* date,contacto* contact)
{
	cita * appointment;
	appointment = (cita*)malloc(sizeof(cita));
	appointment->lugar = lugar;
	appointment->horaInicio = horaIni;
	appointment->horaFin = horaFin;
	appointment->fecha = date  ;
	appointment->contacto = contact;
	return appointment;

}

void citaPrint(Generic g)
{
  cita* appointment = (cita*)g;
	contacto * c = (contacto*)appointment->contacto;
	fecha * f = (fecha*)appointment->fecha;
  fechaPrint(f);
  printf("Lugar:\t %s\nHora Inicio:\t %s\nHora Fin:\t %s\n", appointment->lugar, appointment->horaInicio, appointment->horaFin);
	if (c != NULL)
    printf("Contacto:\t %s \n", c->nombre);
	
}

void citaPrint2(Generic g)
{
  cita* appointment = (cita*)g;
	contacto * c = (contacto*)appointment->contacto;
	fecha * f = (fecha*)appointment->fecha;
	if (c != NULL)
    if (strlen(c->nombre) > 11)
		  printf("%s %s\t", c->nombre, appointment->horaInicio);
    else
      printf("   %s\t %s\t", c->nombre, appointment->horaInicio);
	else
		printf("   Cita   \t%s\t", appointment->horaInicio);
	
}
void citaPrint3(Generic g)
{
  cita* c = (cita*)g;
  printf("Lugar: %s,   ", c->lugar );
  fechaPrint(c->fecha);
}

//compare cita por mes y año
int citaCmpMY(Generic A, Generic B)
{
	fecha* fecha1 = (fecha*)(((cita*)A)->fecha);
	fecha* fecha2 = (fecha*)(((cita*)B)->fecha);

	if (fecha1->year == fecha2->year && fecha1->month == fecha2->month)
		return 0;
	else
		return -1;
}
//compare cita por día
int citaCmpD(Generic A, Generic B)
{
	fecha* fecha1 = (fecha*)(((cita*)A)->fecha);
	fecha* fecha2 = (fecha*)(((cita*)B)->fecha);

	if (fecha1->day == fecha2->day)
		return 0;
	else
		return -1;
}
int citaCmpContact(Generic A, Generic B)
{
  contacto* c1 = (contacto*)(((cita*)A)->contacto);
  contacto* c2 = (contacto*)(((cita*)B)->contacto);
  if(c1 != NULL && c2 != NULL)
  {
    return strcmp(c1->telf, c2->telf);
  }
  else 
    return -1;
}
void calendarioPrint(int mes, List* citas)
{
	  int cont1 = 0;
	  int cont = 0;
  	int cont2 =0;
    int diaIni, numdias, filas = 5;
  	NodeList* nodo;
  	cita* c;
  	char * month;
  	switch(mes)
  	{
  		case 1:
  			month = "ENERO";
        diaIni = 7;
        numdias = 31;
        filas++;
        break;
  		case 2:
  			month = "FEBRERO";
        diaIni = 3;
        numdias = 28;
        break;
  		case 3:
  			month = "MARZO";
        diaIni = 3;
        numdias = 31;
        break;
  		case 4:
  			month = "ABRIL";
        diaIni = 6;
        numdias = 30;
        break;
  		case 5:
  			month = "MAYO";
        diaIni = 1;
        numdias = 31;
        break;
  		case 6:
  			month = "JUNIO";
        diaIni = 4;
        numdias = 30;
        break;
  		case 7:
  			month = "JULIO";
        diaIni = 6;
        numdias = 31;
        filas++;
        break;
  		case 8:
  			month = "AGOSTO";
        diaIni = 2;
        numdias = 31;
        break;
  		case 9:
  			month = "SEPTIEMBRE";
        diaIni = 5;
        numdias = 30;
        break;
		case 10:
  			month = "OCTUBRE";
        diaIni = 7;
        numdias = 31;
        filas++;
        break;
  		case 11:
  			month = "NOVIEMBRE";
        diaIni = 3;
        numdias = 30;
        break;
  		case 12:
  			month = "DICIEMBRE";
        diaIni = 5;
        numdias = 31;
        break;

  	}
	printf("|***********************************************************************************************************************************************************************|\n");
  	printf("|\t\t\t\t\t\t\t\t\t\t" KCYN"%s   " KNRM  "\t\t\t\t\t\t\t\t\t\t|\n", month);
  	printf("|\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t|\n");
	
	printf("|***********************************************************************************************************************************************************************|\n");
  	printf("|"KMAG"\tDOMINGO\t\t" KNRM "|" KMAG"\tLUNES\t\t" KNRM "|" KMAG"\tMARTES\t\t" KNRM "|" KMAG"\tMIERCOLES\t" KNRM "|" KMAG"\tJUEVES\t\t" KNRM "|" KMAG"\tVIERNES\t\t" KNRM "|" KMAG"\tSABADO\t\t" KNRM "|\n");
  	printf("|\t\t\t|\t\t\t|\t\t\t|\t\t\t|\t\t\t|\t\t\t|\t\t\t|\n");
  	printf("|***********************************************************************************************************************************************************************|\n");
  	for (int i = 0; i < filas; i++)
  	{
  		cont2 = 0;
      	for (int j = 0; j < 7; j++)
  		{
  			cont++;
  			if (cont >= diaIni && cont1 < numdias)
  			{
  				cont1++;
          cont2++;
  				printf("|" KWHT "%d" KNRM, cont1);
  				
  			}
  			else
  				printf("|");
  			printf("\t\t\t");
  		}
  		printf("|\n");
      	if(i == 0)
       		cont1 -= 7;
      	else
        	cont1 -= cont2;
  		for (int j = 0; j < 7; j++)
  		{
        	cont1++;
        	c = citaNew(" "," ", " ", fechaNew(cont1, mes, 2017), NULL);
        	nodo = listSearch(citas, (Generic)c, citaCmpD);
        	if(nodo != NULL)
        	{
        	  printf("|");
        	  citaPrint2((cita*)nodo->cont);
        	  listRemoveNode(citas, nodo);
        	}
        	else
  				 printf("|\t\t\t");
  		}
  		printf("|\n");
      	if(i == 0)
      	  	cont1 -= 7;
      	else
        	cont1 -= cont2;
  		for (int j = 0; j < 7; j++)
  		{
        	cont1++;
  				c = citaNew(" "," ", " ", fechaNew(cont1, mes, 2017), NULL);
        	nodo = listSearch(citas, (Generic)c, citaCmpD);
        	if(nodo != NULL)
        	{
        	  printf("|");
        	  citaPrint2((cita*)nodo->cont);
        	  listRemoveNode(citas, nodo);
        	}
        	else
        	 printf("|\t\t\t");
  		}
  		printf("|\n");
      	if(i == 0)
        	cont1 -= 7;
      	else
        cont1 -= cont2;
  		for (int j = 0; j < 7; j++)
  			{
        	cont1++;
  				c = citaNew(" "," ", " ", fechaNew(cont1, mes, 2017), NULL);
        	nodo = listSearch(citas, (Generic)c, citaCmpD);
        	if(nodo != NULL)
        	{
        	  printf("|");
        	  citaPrint2((cita*)nodo->cont);
        	  listRemoveNode(citas, nodo);
        	}
        	else
        	 	printf("|\t\t\t");
  		}
  		printf("|\n");
      	if(i == 0)
      	  	cont1 -= 7;
      	else
        	cont1 -= cont2;
  		for (int j = 0; j < 7; j++)
  			{
        	cont1++;
  				c = citaNew(" "," ", " ", fechaNew(cont1, mes, 2017), NULL);
        	nodo = listSearch(citas, (Generic)c, citaCmpD);
        	if(nodo != NULL)
        	{
        	  printf("|");
        	  citaPrint2((cita*)nodo->cont);
        	  listRemoveNode(citas, nodo);
        	}
        	else
        	 printf("|\t\t\t");
  		}
  		printf("|\n");
  		
  		for (int j = 0; j < 7; j++)
  		{
  			printf("************************");
  		}
  		printf("|\n");
  		
  	}
}

