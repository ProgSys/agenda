#include "../include/contacto.h"
#include <string.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT "\x1B[37m"

contacto* contactoNew(char* nombre, char* apellido, char* telf, char* dir, fecha* birthday)
{
	contacto * contact;
	contact = (contacto*)malloc(sizeof(contacto));
	contact->nombre = nombre;
	contact->apellido = apellido;
	contact->telf = telf;
	contact->birthday = birthday;
	contact->dir = dir;
	return contact;

}

void contactoDetailsPrint(Generic g)
{	
	contacto *c = (contacto*)g;
	printf("%sNombre: %s \nApellido: %s \nTeléfono: %s \nDirección: %s \n",KGRN, c->nombre, c->apellido, c->telf, c->dir);
	
	fechaPrint(c->birthday);
	printf("\n");
}

void contactoPrint(Generic g)
{	
	contacto* c = (contacto*)g;
	printf("%s%s %s\n", KGRN,c->nombre, c->apellido );
}

int cmpContacto (Generic g, Generic h){
	contacto * cont1 = (contacto *) g;
	contacto * cont2 = (contacto *) h;
	if(strcmp(cont1->telf, cont2->telf)==0) 
		return 0;
	return -1;
}