
#ifndef _NODE_H
#define	_NODE_H
#include "generic.h"


/*
Estructura de datos NodeList, el cual es un nodo
para la lista enlazada, donde tiene un puntero Generic
el 
*/
typedef struct NodeList{
    Generic cont;
    struct NodeList *next;
}NodeList;

/*
esta función reserva la memoria para un nodo,
lo crea con el contenido g que se le pase
*/
NodeList *nodeListNew(Generic g);

/*
esta función agrega un contenido a un nodo, 
si este nodo ya tenia un contenido lo remplaza
*/
void nodeListSetCont(NodeList *p, Generic g);

/*
esta función me retorna el contenido de un nodo
*/
Generic nodeListGetCont(NodeList *p);

/*
esta función hace que el puntero next apunte a otro nodo
*/
void nodeListSetNext(NodeList *p, NodeList *q);

/*
esta función me devuelve el nodo Next
*/
NodeList *nodeListGetNext(NodeList *p);


#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* _NODE_H */

