#ifndef _LIST_H
#define	_LIST_H



#include "nodelist.h"


typedef struct List{
    NodeList *header, *last;
}List;


/*
List *listNew(), esta función reserca 
la memoria para una nueva lista, la crea
y la retorna, una lista vacía
*/
List *listNew();


/*
Esta función me devuelve 1 si la lista está vacía, 
caso contrario devuelve cero
*/
int listIsEmpty(List *L);

/*
Esta función me duvuelve el primer nodo de la lista
*/
NodeList *listGetHeader(List *L);


/*
Esta función me duvuelve el último nodo de la lista
*/
NodeList *listGetLast(List *L);



/*
Esta función agrega un nodo a la lista 
*/
void listAddNode(List *L, NodeList *newNode);


/*
Esta función agrega un nodo a la lista en la primera posición
*/
void listAddFirst(List *L, NodeList *newNode);


/*
Esta función agrega un nodo a la lista despues del nodo *p
*/
void listAddNext(List *L, NodeList *p, NodeList *newNode);

/*
Esta función Busca un nodo que segun la funcion de comprara f,
es igual al Generic value, y devuelve el nodo
*/
NodeList *listSearch(List *L, Generic value,cmpfn f);


/*
Esta función Busca los nodos que segun la funcion de comprara f,
son iguales al valor de la variable generic value
esta fucnion retorna una lista
*/ 
List *listSearch2(List *L, Generic value,cmpfn f);      

/*
Esta función, dado un nodo, me devuelve el nodo anterior
*/                                                                                                                                                                                                                                                                                                                                                                  
NodeList *listGetPrevious(List *L, NodeList *p);

/*
Esta función Dado un nodo, lo elimina de la lista
*/
void listRemoveNode(List *L, NodeList *p);


/*
Esta función elimina el primer  nodo de la lista
*/
NodeList *listRemoveFirst(List *L);

/*
Esta función elimina el ultimo de la lista
*/
NodeList *listRemoveLast(List *L);

/*
Esta función devuelve 1, si el nodo p existe en la lista, caso contrario devuelve 0
*/
int listNodeExists(List *L, NodeList *p);

/*
Esta función me retorna el tamaño de la lista
*/
int listGetSize(List *L);

/*
esta función elimina la lista
*/
void listDelete(List **L);

/*
esta función imprime la lista nodo por nodo, 
según la función print que se le pase como parametro
*/
void listPrint(List *L, printfn print);


List *listReadFile(char *fileName, readfn read);

/*
esta función imprime el elemento numero "number" de la lista, en base a la función print
*/
void listByNumber(List *L, printfn print, int number);

/*
esta función elimina el elemento numero "numbre" de la lista
*/
void deleteByNumber (List *L, int number);

/*
esta función me devuelve el nodo numero "number de la lista"
*/
NodeList * listSearchByNumber(List *L, int number);

/** @} */
#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* _LIST_H */

