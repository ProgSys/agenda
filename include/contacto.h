#include "list.h"
#include "fecha.h"

/*
Esta es la estructura contacto, 
la cual cuenta con 4 char* para:
 -nombre
 -apellido
 -telefono
 -dirección
 -fecha de nacimiento (el cual es del tipo de dato fecha)
*/
typedef struct Tcontacto{
  char * nombre;
  char * apellido;
  char * telf;
  char * dir;
  fecha * birthday;
}contacto;


/*
Esta función reserva la memoria para un nuevo contacto, 
crea el contacto y lo retorna
*/
contacto* contactoNew(char*, char*, char*,char* , fecha*);

/*
esta función imprime un contacto, el cual es apuntado por 
un puntero Generic (void *)
*/
void contactoPrint(Generic );
 
/*
esta función imprime un contacto a detalle, el cual es apuntado por 
un puntero Generic (void *)
*/ 
void contactoDetailsPrint(Generic);

/*
esta función compara dos contactos apuntados por un puntero Generic (void*)
cada uno, la función devuelve 0 si son iguales y -1 si son diferentes
*/
int cmpContacto (Generic , Generic );



