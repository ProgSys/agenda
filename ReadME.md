#PoliAgenda

## Agenda de citas y contactos

Proyecto Parcial de Programación de Sistemas. FIEC-ESPOL.

1. Versión: 0.1
2. Autores:
*Jorge Cedeño ~ Fabricio Layedra*
3. Lenguaje: C
4. Fecha: Junio 30 de 2017

## Descripción

El programa consiste en un registro de contactos y citas. Cada *Contacto* consta de la siguiente información: **NOMBRE, APELLIDO, DIRECCIÓN, TELÉFONO, FECHA DE NACIMIENTO y CITAS ASOCIADAS** en caso de tenerlas. A su vez, cada *Cita* consta de la siguiente información: **LUGAR, HORA DE INICIO, HORA DE FIN, FECHA** y como elemento opcional, un **CONTACTO ASOCIADO**. EL dueño de la agenda puede crear, modificar y borrar contactos, así como ver el detalle de cada uno. Además, puede crear, modificar y borrar citas; y verlas tanto en formato calendario mensual como en detalle.

## Plataforma

EL código fuente ha sido desarrollado en lenguaje C y la persistencia de datos se maneja mediante archivos JSON. La lectura y escritura de estos archivos se realiza mediante las librerías [jRead](https://goo.gl/a1NfVJ) y [jWrite](https://goo.gl/mZBbHv). Se recomienda utilizar pantallas HD para la ejecución del programa.

##Recursos

El proyecto contiene 4 directorios, 2 librerías estáticas, 1 Makefile y el presente ReadME.

###Directorios:
1. ***src***: Aloja el programa principal en el archivo *main.c*.
2. ***lib***: Aloja las librerías que utiliza el sistema para ejecutarse.
3. ***include***: Aloja los archivos de cabecera del sistema.
4. ***data***: Aloja los archivos JSON donde se almacenan los *contactos* y *citas* registrados en el sistema.

###Librerías:
1. ***jsonRLib.a***: Librería para la lectura de archivos JSON.
2. ***jsonLib.a***: Librería para la escritura de archivos JSON.

###Makefile:

Compila de forma efectiva los recursos del sistema. Escriba y ejecute **make** en su terminal para generar el archivo ejecutable del sistema. Para limpiar el directorio de archivos ejecutables obsoletos, escriba y ejecute **make clean** en su terminal.

##Flujo de Navegación

El programa inicia con un mensaje de bienvenida y un menú principal de tres opciones. Debe ingresar 1, 2 o 3 para poder continuar.

![1PS.png](https://bitbucket.org/repo/5qdGBka/images/1369417077-1PS.png)

1. En el caso de **1**: Ingresará al menú de opciones para *contactos*.
2. En el caso de **2**: Ingresará el menú de opciones para *citas*.
3. En el caso de **3**: Sale del sistema.

###Contactos

En la opción *Contactos*, al inicio se presentarán los contactos registrados en la agenda hasta la fecha. Luego, un menú de opciones como se observa a continuación:

![2PS.png](https://bitbucket.org/repo/5qdGBka/images/1490484903-2PS.png)

Luego de elegir una opción, se le solicitará el número de contacto, que es la posición en la lista presentada al inicio.

Cada vez que presenta información, le pedirá ingresar 0 para volver al menú principal y seguir con la ejecución del programa.

###Citas

En la opción *Citas*, al inicio se presentarán las citas registradas en la agenda hasta la fecha. Luego, un menú de opciones como se observa a continuación:

![3PS.png](https://bitbucket.org/repo/5qdGBka/images/3056117367-3PS.png)

Luego de elegir una opción, se le solicitará el número de cita, que es la posición en la lista presentada al inicio.

Cada vez que presenta información, le pedirá ingresar 0 para volver al menú principal y seguir con la ejecución del programa.

![4PS.png](https://bitbucket.org/repo/5qdGBka/images/4039554261-4PS.png)

En la opción *Ver citas en formato calendario*, necesita además ingresar el mes como un número [1-12]. Dado que esta agenda es anual, las citas solo deben registrarse para el año 2017 para poder verlas.