#include <stdio.h>

#include "../include/jWrite.h"
#include "../include/jRead.h"
#include "../include/cita.h"
#define NAMELEN 50

void writeJSON(List*L){
    char buffer[2048];
    unsigned int buflen = 2048;
    jwOpen(buffer, buflen, JW_OBJECT, JW_PRETTY);
    int i;
    NodeList * iterator;
    contacto * tempC;
    cita* tempCita;
    jwObj_array( "contactos"); 
    int err;
    for( iterator = L->header; iterator!=NULL; iterator = iterator->next)    // loop for no. of elements in JSON
        { 
            tempC = (contacto*) iterator->cont;
            jwArr_object();
              jwObj_string("nombre",tempC->nombre);
              jwObj_string("apellido",tempC->apellido);
              jwObj_string("telefono",tempC->telf);
              jwObj_string("direccion",tempC->dir);
              jwObj_array("fecha");
                jwArr_int(tempC->birthday->day);                              // add a few integers to the array
                jwArr_int(tempC->birthday->month);
                jwArr_int( tempC->birthday->year );
              jwEnd();                                  // add a few integers to the arra
            jwEnd(); 
            
        }
    jwEnd();                                      // end the array
    err= jwClose();  
    char * file = "./data/contact.json";
    FILE *fp = fopen(file , "w");
    fprintf(fp, "%s", buffer );
    fclose(fp);
}

int readJSON(List * contactos, List* citas){

    char *buffer;
    long numbytes;
    FILE *fp;
    fp = fopen("./data/data.json", "r");

    if(fp == NULL)
    return 2;

    fseek(fp,0L, SEEK_END);
    numbytes= ftell(fp);

    fseek(fp, 0L, SEEK_SET);

    buffer = (char *) calloc (numbytes, sizeof(char));

    if (buffer==NULL) return 1;

    fread(buffer, sizeof(char), numbytes, fp);
    fclose(fp);

    
    fecha* dateT;
    contacto* conTemp;
    Generic gTemp;
    cita* citaTemp;
    NodeList *nodoT = NULL;
    char nombreT [NAMELEN];
    char apellidoT [NAMELEN];
    char telefonoT [NAMELEN];
    char direccionT [NAMELEN];
    char lugarT [NAMELEN];
    char horaInicioT [NAMELEN];
    char horaFinT [NAMELEN];
    char numeroT [NAMELEN];
    int dayT;
    int monthT;
    int yearT; 
    struct jReadElement element;
    int i;
    jRead( buffer, "{'contactos'", &element );    // we expect "Numbers" to be an array
    if( element.dataType == JREAD_ARRAY ) 
    {
        for( i=0; i<element.elements; i++ )    // loop for no. of elements in JSON
        { 
            jRead_string( buffer, "{'contactos'[*{'nombre'", nombreT, NAMELEN, &i );
            jRead_string( buffer, "{'contactos'[*{'apellido'", apellidoT, NAMELEN, &i );
            jRead_string( buffer, "{'contactos'[*{'telefono'", telefonoT, NAMELEN, &i );
            jRead_string( buffer, "{'contactos'[*{'direccion'", direccionT, NAMELEN, &i );
            dayT= jRead_int( buffer, "{'contactos'[*{'fecha'[0" , &i);
            monthT= jRead_int( buffer, "{'contactos'[*{'fecha'[1" , &i);
            yearT= jRead_int( buffer, "{'contactos'[*{'fecha'[2" , &i);
            dateT = fechaNew(dayT,monthT,yearT);
            conTemp = contactoNew(strdup(nombreT), strdup(apellidoT), strdup(telefonoT), strdup(direccionT), dateT);
            gTemp = (Generic) conTemp;
            nodoT = nodeListNew(gTemp);
            listAddNode(contactos,nodoT);
            
        }
    }
    jRead( buffer, "{'citas'", &element );    // we expect "Numbers" to be an array
    if( element.dataType == JREAD_ARRAY ) 
    {
        for( i=0; i<element.elements; i++ )    // loop for no. of elements in JSON
        { 
            jRead_string( buffer, "{'citas'[*{'lugar'", lugarT, NAMELEN, &i );
            jRead_string( buffer, "{'citas'[*{'horaInicio'", horaInicioT, NAMELEN, &i );
            jRead_string( buffer, "{'citas'[*{'horaFin'", horaFinT, NAMELEN, &i );
            jRead_string( buffer, "{'citas'[*{'contacto'", numeroT, NAMELEN, &i );
            dayT= jRead_int( buffer, "{'citas'[*{'fecha'[0" , &i);
            monthT= jRead_int( buffer, "{'citas'[*{'fecha'[1" , &i);
            yearT= jRead_int( buffer, "{'citas'[*{'fecha'[2" , &i);
            dateT = fechaNew(dayT,monthT,yearT);
            if(strcmp(numeroT, "") != 0)
            {
              conTemp = contactoNew(" ", " ", strdup(numeroT), " ", NULL);
              conTemp = (contacto*)listSearch(contactos, (Generic)conTemp, cmpContacto)->cont;
              citaTemp = citaNew(strdup(lugarT), strdup(horaInicioT), strdup(horaFinT), dateT, conTemp);
            }
            else
              citaTemp = citaNew(strdup(lugarT), strdup(horaInicioT), strdup(horaFinT), dateT, NULL);
            gTemp = (Generic)citaTemp;
            nodoT = nodeListNew(gTemp);
            listAddNode(citas,nodoT);
 
        }
    }
    return 0;
}

int main()
{

    cita* citan;
  	List* agenda= listNew();
    List* citas = listNew();
    List* citas2;
    int day, month, year;
    Generic g;
    contacto * c =  NULL;
    NodeList *nodo = NULL;
    char name[50], apellido[50], numero[50], dir[50], lug[50], hini[50], hfin[50];
    
    readJSON(agenda, citas);

    int option = 0, opt = 0, opt2=0, chosen = 0, mes, chosen2=0, x;
    
    do
    {
      system("clear");
      printf("%s|*****Bienvenido a tu Agenda.*****|\n",KCYN);
      printf("%s1. Contactos.\n",KWHT);
      printf("2. Citas.\n");
      printf("3. Salir.\n");
      printf("Indica la opción: \n");
      scanf ( "%d", &option);
      switch(option)
      {
        case 1:
            do
            {
                
                opt = 0;
                //system("clear");
                printf("%s------LISTA DE CONTACTOS------\n",KCYN);
                listPrint(agenda,contactoPrint);
                printf("%s------------------------------\n",KCYN);
                printf("%s1. Ver detalle de contacto.\n",KWHT);
                printf("2. Agregar contacto.\n");
                printf("3. Editar contacto.\n");
                printf("4. Borrar contacto.\n");
                printf("5. Salir.\n" );
                printf("Indica la opción: \n");
                scanf ( "%d", &opt);
                switch(opt)
                {
                    case 1:
                        printf("%s\n", "Escoge el número del contacto: ");
                        scanf ( "%d", &chosen);
                        listByNumber(agenda, contactoDetailsPrint, chosen);
                        nodo = listSearchByNumber(agenda, chosen);
                        citan = citaNew(" ", " ", " ", NULL, (contacto*)nodo->cont);
                        citas2 = listSearch2(citas, (Generic)citan, citaCmpContact);
                        if(!listIsEmpty(citas2))
                        {
                          printf("Citas:\n");
                          listPrint(citas2, citaPrint3);
                        }
                        printf("\n\nIngrese el cero para regresar al menú anterior: \t");
                        scanf("%d", &x);
                        //writeJSON(agenda);


                        break;     
                    case 2:
                        getchar();
                        memset(name,'\0',50);
                        printf("Ingrese nombre de contacto: ");
                        scanf("%[^\n]s", name);
                        getchar();
                        //printf("%s\n",name);
                        
                        memset(apellido,'\0',50);
                        printf("Ingrese apellido de contacto: ");
                        scanf ("%[^\n]s",apellido);
                        getchar();
                        //printf("%s\n",apellido);

                        memset(numero,'\0',50);
                        printf("Ingrese número de contacto: ");
                        scanf ("%[^\n]s",numero);
                        getchar();
                        //printf("%s\n",numero);

                        memset(dir,'\0',50);
                        printf("Ingrese dirección: ");
                        scanf ("%[^\n]s",dir);
                        getchar();

                        printf("Ingrese fecha de nacimiento. \n");
                        printf("Día: ");
                        scanf ( "%d", &day);                
                        printf("Mes: ");
                        scanf ( "%d", &month);
                        printf("Año: ");
                        scanf ( "%d", &year);
                     
                        listAddNode(agenda, nodeListNew((Generic)contactoNew(strdup(name), strdup(apellido), strdup(numero), strdup(dir), fechaNew(day, month, year))));
                        system("clear");
                        break;

                     case 3:

 

                         printf("%s\n", "Escoge el número del contacto: ");
                         scanf ( "%d", &chosen);
                         printf("%s%s\n", KRED,"Contacto por Editar" );
                   
                         listByNumber(agenda, contactoDetailsPrint, chosen);
                         getchar();
                         memset(name,'\0',50);
                         printf("%sIngrese nuevo nombre de contacto: ", KRED);
                         scanf("%[^\n]", name);
                         //printf("%s\n",name);
                         getchar();                    
                         memset(apellido,'\0',50);
                         printf("Ingrese nuevo apellido de contacto: ");
                         scanf ( "%[^\n]",apellido);
                         //printf("%s\n",apellido);
                         getchar();
                         memset(numero,'\0',50);
                         printf("Ingrese nuevo número de contacto: ");
                         scanf ( "%[^\n]",numero);
                         //printf("%s\n",numero);
                         getchar();
                         memset(dir,'\0',50);
                         printf("Ingrese nueva dirección: ");
                         scanf ( "%[^\n]",dir);
                         getchar();

                         printf("Ingrese nueva fecha de nacimiento. \n");
                         printf("Año: ");

                         int yearn;
                         scanf ( "%d", &yearn);
                         //printf("%d\n",anio);
                         printf("Mes: ");

                         int monthn;
                         scanf ( "%d", &monthn);
                         printf("Día: ");

                         int dayn;
                         scanf ( "%d", &dayn);

                         if(listSearchByNumber(agenda, chosen)!=NULL)
                         {
                           c = (contacto*)listSearchByNumber(agenda, chosen)->cont;
                           c->nombre = strdup(name);
                           c->apellido = strdup(apellido);
                           c->telf = strdup(numero);
                           c->dir = strdup(dir);
                           c->birthday = fechaNew(dayn, monthn, yearn);

                         }                                   
                         break;
                     case 4:

                        printf("%s\n", "Escoge el número del contacto: ");
                        scanf ( "%d", &chosen);
                        deleteByNumber(agenda, chosen);
                        system("clear");

                        break;
                }
            }while(opt!=5); 
            break;         
        case 2:
          do
          {
            system("clear");
            printf("%s---------LISTA DE CITAS---------\n",KCYN);
            listPrint(citas, citaPrint3);
            printf("%s--------------------------------\n",KCYN);
            printf("1. Agregar Nueva Cita.\n");
            printf("2. Modificar cita.\n");
            printf("3. Eliminar Cita.\n");
            printf("4. Ver una cita.\n");
            printf("5. Ver citas en formato calendario.\n");
            printf("6. Salir.\n" );
            printf("Indica la opción: \n");
            scanf ( "%d", &opt2);
            switch(opt2)
            {
              //En caso de agregar una nueva cita, se piden todos los datos de la cita
              case 1:
                getchar();
                memset(lug,'\0',50);
                printf("\nIngrese el Lugar de la cita:\t");
                scanf("%s",lug);
                getchar();
                printf("Ingrese fecha de reunión. \n");
                printf("Día: ");
                scanf ( "%d", &day);                
                printf("Mes: ");
                scanf ( "%d", &month);
                printf("Año: ");
                scanf ( "%d", &year);
                memset(hini,'\0',50);
                printf("\nIndique la hora de inicio (HH:MM):\t");
                scanf("%s",hini);
                getchar();
                memset(hfin,'\0',50);
                printf("\nIndique la hora de fin (HH:MM):   \t");
                scanf("%s",hfin);
                getchar();
                printf("%s------LISTA DE CONTACTOS------\n",KCYN);
                listPrint(agenda,contactoPrint);
                printf("%s------------------------------\n",KCYN);
                printf("\n Indique con quién es la cita, en caso de que no desee agregar un contacto a la cita ingrese 0: \t");
                scanf("%d", &chosen);
                if(chosen != 0)  
                  c = (contacto*)listSearchByNumber(agenda, chosen)->cont;
                else 
                  c = NULL;
                listAddNode(citas, nodeListNew((Generic)citaNew(strdup(lug), strdup(hini), strdup(hfin), fechaNew(day, month, year), c )));
                break;
              case 2:
                //Seleccionar cita a modificar
                printf("%s\n", "Escoge el número del cita: ");
                scanf ( "%d", &chosen2);
                printf("%s%s\n", KRED,"Cita por Editar" );
          
                
                //Imprimir cita seleccionada
                listByNumber(citas, citaPrint, chosen2);
                //Pedir nuevos datos para la cita
                getchar();
                memset(lug,'\0',50);
                printf("\nIngrese el Lugar de la cita:\t");
                scanf("%s",lug);
                getchar();
                printf("Ingrese fecha de reunión. \n");
                printf("Día: ");
                scanf ( "%d", &day);                
                printf("Mes: ");
                scanf ( "%d", &month);
                printf("Año: ");
                scanf ( "%d", &year);
                memset(hini,'\0',50);
                printf("\nIndique la hora de inicio (HH:MM):\t");
                scanf("%s",hini);
                getchar();
                memset(hfin,'\0',50);
                printf("\nIndique la hora de fin (HH:MM):   \t");
                scanf("%s",hfin);
                getchar();
                printf("%s------LISTA DE CONTACTOS------\n",KCYN);
                listPrint(agenda,contactoPrint);
                printf("%s------------------------------\n",KCYN);
                printf("\n Indique con quién es la cita, en caso de que no desee agregar un contacto a la cita ingrese 0: \t");
                scanf("%d", &chosen);
                if (chosen != 0)
                  c = (contacto*)listSearchByNumber(agenda, chosen)->cont;
                else
                  c=NULL;
                //Obtener cita de la lista, cambiarle los datos
                citan = (cita*)listSearchByNumber(citas, chosen2)->cont;
                citan->lugar = strdup(lug);
                citan->horaInicio = strdup(hini);
                citan->horaFin = strdup(hfin);
                citan->fecha = fechaNew(day, month, year);
                citan->contacto = c;

                break;
              case 3:
                //Eliminar una cita
                printf("%s\n", "Escoge el número de cita: ");
                scanf ( "%d", &chosen2);
                deleteByNumber(citas, chosen2);
                printf("\n\nIngrese el cero para regresar al menú anterior: \t");
                scanf("%d", &x);
                break;
              case 4:
                printf("%s\n", "Escoge el número de cita: ");
                scanf ( "%d", &chosen2);
                listByNumber(citas, citaPrint, chosen2);
                break;

                //Ver una Cita
              case 5:
                printf("Indique el mes que desea ver:\n");
                scanf("%d", &mes); 
                citan = citaNew(" ", " ", " ", fechaNew(1, mes, 2017), NULL);
                citas2 = listSearch2(citas, (Generic)citan, citaCmpMY);
                calendarioPrint(mes, citas2);
                printf("\n\nIngrese el cero para regresar al menú anterior: \t");
                scanf("%d", &x);
                break;
            }
          }while(opt2!=6);
          break;
      }
    }while(option!=3);
    writeJSON(agenda);
    printf("%s\n", "¡Has salido de tu agenda!" );

  
}

/*

gcc src/main.c lib/cita.c lib/list.c lib/contacto.c lib/generic.c lib/fecha.c lib/nodelist.c include/cita.h include/list.h include/contacto.h include/generic.h include/fecha.h include/nodelist.h

*/
