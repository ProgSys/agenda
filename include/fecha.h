#include <stdlib.h>
#include <stdio.h>


/*
Esta es la estructura fecha
contiene 3 parámetros enteros
día, mes y año.

*/
typedef struct Tfecha{
	int day;
	int month;
	int year; 
}fecha;

/*
Esta funcíon reserva memoria para una nueva fecha 
y me retorna dicha fecha
*/
fecha* fechaNew(int, int, int);


/*
Esta función me imprime una fecha
*/
void fechaPrint(fecha*); 

/*
Esta función me devuelve la fecha en forma de string
*/
char* fechaGetString(fecha*);


