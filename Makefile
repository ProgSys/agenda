
IDIR =include

CC=gcc
CFLAGS=-I$(IDIR)

ODIR=obj
LDIR =lib

SRCDIR = src



LIBS=-lm

_DEPS = cita.h list.h contacto.h generic.h fecha.h nodelist.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_SRC = main.c
SRC = $(patsubst %,$(SRCDIR)/%,$(_SRC))

_OBJ = cita.c list.c contacto.c generic.c fecha.c nodelist.c 
OBJ = $(patsubst %,$(LDIR)/%,$(_OBJ))

JWRITE = jsonLib.a
JREAD = jsonRLib.a

make: 
	$(CC) -g -static -o $@ $(SRC) $(OBJ) $(JWRITE) $(JREAD) $(CFLAGS) $(LIBS)
makemain:
	gcc src/main.c lib/cita.c lib/list.c lib/contacto.c lib/generic.c lib/fecha.c lib/nodelist.c lib/jRead.c lib/jWrite.c include/cita.h include/list.h include/contacto.h include/generic.h include/fecha.h include/nodelist.h include/jWrite.h include/jRead.h

.PHONY: clean

clean:
	rm -f make
	rm -f $(LDIR)/*.o 
	rm -f $(SRCDIR)/*.o 
	rm -f $(IDIR)/*.gch
	
